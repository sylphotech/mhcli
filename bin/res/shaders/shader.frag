#version 430

out vec4 color;

vec3 hsb2rgb(in vec3 c) {
    vec3 rgb = clamp(
		abs(
			mod(c.x * 6.0 + vec3(0.0, 4.0, 2.0), 6.0) - 3.0
		) -1.0, 0.0, 1.0
	);
    
	rgb = rgb * rgb * (3.0 - 2.0 * rgb);
    return c.z * mix(vec3(1.0), rgb, c.y);
}

void main() {
	vec2 st = gl_FragCoord.xy/vec2(1000, 1000);
	vec3 fcolor = vec3(0, 0, 0);
	vec2 toCenter = vec2(0.5) - st;
	float angle = atan(toCenter.y, toCenter.x);
    float radius = length(toCenter) * 2.0;
	fcolor = hsb2rgb(vec3((angle / 6.28318530718) + 0.5, radius, 1.0));
	color = vec4(fcolor, 1.0);
}
