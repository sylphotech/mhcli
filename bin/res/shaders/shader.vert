#version 430

in vec3 position;
in vec3 normal;
out vec4 pos;
uniform mat4 u_perspective;
uniform mat4 u_view;
uniform mat4 u_model;

void main() {
	mat4 mvp = u_perspective * u_view * u_model;
	pos = gl_Position = (mvp * vec4(position, 1));
}
