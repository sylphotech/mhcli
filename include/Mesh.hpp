#pragma once

namespace mesh
{
    struct vertex
    {
        glm::fvec3 position;
        glm::fvec3 normal;

        vertex(glm::fvec3 pos) : position(pos) {
            
        }
    };

    struct mesh
    {
        std::vector<vertex> verts;
        std::vector<int> indices;
    };
}
