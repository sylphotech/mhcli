#pragma once

#include <GL/glew.h>
#include <vector>
#include <string>

#include "io.hpp"

namespace shader {
    /*
        Loads a shader from a file
    */
    GLuint load(const char* shader_path, GLenum shaderType)
    {
        GLuint shaderID = glCreateShader(shaderType);

        // Read the Fragment Shader code from the file
        std::string shaderCode;
        if (!io::file::to_string(shaderCode, shader_path)) return -1;

        // Compile Shader
        char const *sourcePointer = shaderCode.c_str();
        glShaderSource(shaderID, 1, &sourcePointer, NULL);
        glCompileShader(shaderID);

        // Check Shader
        GLint Result = GL_FALSE;
        int InfoLogLength;

        glGetShaderiv(shaderID, GL_COMPILE_STATUS, &Result);
        glGetShaderiv(shaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
        if (InfoLogLength > 0)
        {
            std::vector<char> shaderErrorMessage(InfoLogLength+1);
            glGetShaderInfoLog(shaderID, InfoLogLength, NULL, &shaderErrorMessage[0]);
            printf("%s\n", &shaderErrorMessage[0]);
            return -1;
        }

        return shaderID;
    }

    /*
        Links shaders to a program and links
    */
    GLuint link(std::vector<GLuint> shaders)
    {
        GLuint program = glCreateProgram();

        for (int shader : shaders)  glAttachShader(program, shader);

        glLinkProgram(program);

        // Check the program
        GLint Result = GL_FALSE;
        int InfoLogLength;

        glGetProgramiv(program, GL_LINK_STATUS, &Result);
        glGetProgramiv(program, GL_INFO_LOG_LENGTH, &InfoLogLength);
        if (InfoLogLength > 0) {
            std::vector<char> ProgramErrorMessage(InfoLogLength+1);
            glGetProgramInfoLog(program, InfoLogLength, NULL, &ProgramErrorMessage[0]);
            printf("%s\n", &ProgramErrorMessage[0]);
            return -1;
        }

        for (int shader : shaders)  glDetachShader(program, shader);

        return program;
    }
}
