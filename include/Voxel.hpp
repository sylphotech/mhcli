#pragma once

#include <GLM/glm.hpp>

// Class handling voxel data.
struct Voxel final {
    glm::vec3 position;

    Voxel() = default;
    ~Voxel() = default;
};