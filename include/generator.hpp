#pragma once

#include "noise/perlin.hpp"
#include <iostream>

namespace generator
{
    const siv::PerlinNoise perlin(65535);

    float gen (float x, float y, float z)
    {
        return perlin.octaveNoise0_1(x / 2, y / 2, z / 2, 8.0f);
    }

    float gen(const glm::fvec3& vec) {
        return perlin.octaveNoise0_1(vec.x / 2, vec.y / 2, vec.z / 2, 8.0f);
    }
}
