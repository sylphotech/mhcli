#pragma once

#include <string>
#include <cstdio>

namespace io {
    namespace file {
        bool to_string(std::string& target, const char *filename)
        {
            std::cout << "Reading file: " << std::endl;
            std::FILE *fp = std::fopen(filename, "rb");
            if (fp)
            {
                std::fseek(fp, 0, SEEK_END);
                target.resize(std::ftell(fp));
                std::rewind(fp);
                std::fread(&target[0], 1, target.size(), fp);
                std::fclose(fp);
                return true;
            }

            return false;
        }
    }

    namespace net {

    }
}
