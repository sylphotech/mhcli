#include <vector>
#include <iostream>

#include <GLM/glm.hpp>
#include "marching_cubes_tables.h"
#include "mesh.hpp"
#include "generator.hpp"

namespace march
{
    glm::fvec3 VertexInterp(
        double isolevel,
        glm::fvec3 p1,
        glm::fvec3 p2,
        double valp1,
        double valp2
    ) {
        double mu;
        glm::fvec3 p;

        if (glm::abs(isolevel - valp1) < 0.00001)
            return(p1);
        if (glm::abs(isolevel - valp2) < 0.00001)
            return(p2);
        if (glm::abs(valp1 - valp2) < 0.00001)
            return(p1);

        mu = (isolevel - valp1) / (valp2 - valp1);
        p.x = p1.x + mu * (p2.x - p1.x);
        p.y = p1.y + mu * (p2.y - p1.y);
        p.z = p1.z + mu * (p2.z - p1.z);

        return(p);
    }

    mesh::mesh* run(
        const glm::fvec3 chunk_size,
        const float sclr
    ) {
        const float ssclr = sclr / 2;

        mesh::mesh* chunk = new mesh::mesh();
        for (float x = 0; x < chunk_size.x; x += sclr)
        {
            for (float y = 0; y < chunk_size.y; y += sclr)
            {
                for (float z = 0; z < chunk_size.z; z += sclr)
                {
                    // std::cout << x << " " << y << " " << z << std::endl;

                    float isolevel = 0.5f;
                    float xps = x + ssclr;
                    float yps = y + ssclr;
                    float zps = z + ssclr;
                    float xns = x - ssclr;
                    float yns = y - ssclr;
                    float zns = z - ssclr;

                    glm::fvec3 v0(xns, yns, zns);
                    glm::fvec3 v1(xps, yns, zns);
                    glm::fvec3 v2(xps, yns, zps);
                    glm::fvec3 v3(xns, yns, zps);
                    glm::fvec3 v4(xns, yps, zns);
                    glm::fvec3 v5(xps, yps, zps);
                    glm::fvec3 v6(xps, yps, zns);
                    glm::fvec3 v7(xns, yps, zns);

                    // glm::fvec3 v0(x,  y,   z);
                    // glm::fvec3 v1(xs, y,   z);
                    // glm::fvec3 v2(xs, y,  zs);
                    // glm::fvec3 v3(x,  y,  zs);
                    // glm::fvec3 v4(x,  ys,  z);
                    // glm::fvec3 v5(xs, ys,  z);
                    // glm::fvec3 v6(xs, ys, zs);
                    // glm::fvec3 v7(x,  ys, zs);

                    // Generate the value at each point
                    float pn0 = generator::gen(v0);
                    float pn1 = generator::gen(v1);
                    float pn2 = generator::gen(v2);
                    float pn3 = generator::gen(v3);
                    float pn4 = generator::gen(v4);
                    float pn5 = generator::gen(v5);
                    float pn6 = generator::gen(v6);
                    float pn7 = generator::gen(v7);

                    std::cout << "PERLIN0: " << pn0 << "\n";
                    std::cout << "PERLIN1: " << pn1 << "\n";
                    std::cout << "PERLIN2: " << pn2 << "\n";
                    std::cout << "PERLIN3: " << pn3 << "\n";
                    std::cout << "PERLIN4: " << pn4 << "\n";
                    std::cout << "PERLIN5: " << pn5 << "\n";
                    std::cout << "PERLIN6: " << pn6 << "\n";
                    std::cout << "PERLIN7: " << pn7 << "\n";

                    // Here we get our edge colision index.
                    unsigned int config_n = 0;
                    if (pn0 < isolevel) {
                        config_n |= 1 << 0;
                        std::cout << "BIT0 SET\n";
                    }
                    if (pn1 < isolevel) {
                        config_n |= 1 << 1;
                        std::cout << "BIT1 SET\n";
                    }
                    if (pn2 < isolevel) {
                        config_n |= 1 << 2;
                        std::cout << "BIT2 SET\n";
                    }
                    if (pn3 < isolevel) {
                        config_n |= 1 << 3;
                        std::cout << "BIT3 SET\n";
                    }
                    if (pn4 < isolevel) {
                        config_n |= 1 << 4;
                        std::cout << "BIT4 SET\n";
                    }
                    if (pn5 < isolevel) {
                        config_n |= 1 << 5;
                        std::cout << "BIT5 SET\n";
                    }
                    if (pn6 < isolevel) {
                        config_n |= 1 << 6;
                        std::cout << "BIT6 SET\n";
                    }
                    if (pn7 < isolevel) {
                        config_n |= 1 << 7;
                        std::cout << "BIT7 SET\n";
                    }

                    // if point has no intersections, or we're in object, skip.
                    if (config_n == 0 || config_n == 255) continue;

                    // get config
                    std::cout << "Config_n: " << config_n << "\n";
                    unsigned int intersected_edges = edgeTable[config_n];
                    std::cout << "Edge table value hex: " << intersected_edges << "\n";
                    glm::fvec3 points[12];

                    // **Issue probably in this section below but not sure why:**
                    // Find intersecting point vertices
                    // ssclr is short for semi-scaler. (half of scaler)
                    // here we are getting the edge verts and offsetting by xyz
                    /*if (intersected_edges &    1) points[0] = glm::fvec3(x + ssclr, y + 0,     z + 0    );
                    if (intersected_edges &    2) points[1] = glm::fvec3(x + sclr,  y + 0,     z + ssclr);
                    if (intersected_edges &    4) points[2] = glm::fvec3(x + ssclr, y + 0,     z + sclr );
                    if (intersected_edges &    8) points[3] = glm::fvec3(x + 0,     y + 0,     z + ssclr);

                    if (intersected_edges &   16) points[4] = glm::fvec3(x + ssclr, y + sclr,  z + 0    );
                    if (intersected_edges &   32) points[5] = glm::fvec3(x + sclr,  y + sclr,  z + ssclr);
                    if (intersected_edges &   64) points[6] = glm::fvec3(x + ssclr, y + sclr,  z + sclr );
                    if (intersected_edges &  128) points[7] = glm::fvec3(x + 0,     y + sclr,  z + ssclr);

                    if (intersected_edges &  256) points[8] = glm::fvec3(x + sclr,  y + ssclr, z + 0    );
                    if (intersected_edges &  512) points[9] = glm::fvec3(x + sclr,  y + ssclr, z + ssclr);
                    if (intersected_edges & 1024) points[10]= glm::fvec3(x + sclr,  y + ssclr, z + sclr );
                    if (intersected_edges & 2048) points[11]= glm::fvec3(x + 0,     y + ssclr, z + ssclr);*/

                    if (intersected_edges & 1 <<  0) {
                        // points[ 0] = VertexInterp(isolevel, v0, v1, pn0, pn1);
                        points[0] = glm::fvec3(x, yns, zns);
                        std::cout << "IEBIT0 SET\n";
                    }
                    if (intersected_edges & 1 <<  1) {
                        // points[ 1] = VertexInterp(isolevel, v1, v2, pn1, pn2);
                        points[1] = glm::fvec3(xps, yns, z);
                        std::cout << "IEBIT1 SET\n";
                    }
                    if (intersected_edges & 1 <<  2) {
                        // points[ 2] = VertexInterp(isolevel, v2, v3, pn2, pn3);
                        points[2] = glm::fvec3(x, yns, zps);
                        std::cout << "IEBIT2 SET\n";
                    }
                    if (intersected_edges & 1 <<  3) {
                        // points[ 3] = VertexInterp(isolevel, v3, v0, pn3, pn0);
                        points[3] = glm::fvec3(xns, yns, z);
                        std::cout << "IEBIT3 SET\n";
                    }
                    if (intersected_edges & 1 <<  4) {
                        // points[ 4] = VertexInterp(isolevel, v4, v5, pn4, pn5);
                        points[4] = glm::fvec3(x, yps, zns);
                        std::cout << "IEBIT4 SET\n";
                    }
                    if (intersected_edges & 1 <<  5) {
                        // points[ 5] = VertexInterp(isolevel, v5, v6, pn5, pn6);
                        points[5] = glm::fvec3(xps, yps, z);
                        std::cout << "IEBIT5 SET\n";
                    }
                    if (intersected_edges & 1 <<  6) {
                        // points[ 6] = VertexInterp(isolevel, v6, v7, pn6, pn7);
                        points[6] = glm::fvec3(x, yps, zps);
                        std::cout << "IEBIT6 SET\n";
                    }
                    if (intersected_edges & 1 <<  7) {
                        // points[ 7] = VertexInterp(isolevel, v7, v4, pn7, pn0);
                        points[7] = glm::fvec3(xns, yps, z);
                        std::cout << "IEBIT7 SET\n";
                    }
                    if (intersected_edges & 1 <<  8) {
                        // points[ 8] = VertexInterp(isolevel, v0, v4, pn0, pn4);
                        points[8] = glm::fvec3(xns, y, zns);
                        std::cout << "IEBIT8 SET\n";
                    }
                    if (intersected_edges & 1 <<  9) {
                        // points[ 9] = VertexInterp(isolevel, v1, v5, pn1, pn5);
                        points[9] = glm::fvec3(xps, y, zns);
                        std::cout << "IEBIT9 SET\n";
                    }
                    if (intersected_edges & 1 << 10) {
                        // points[10] = VertexInterp(isolevel, v2, v6, pn2, pn6);
                        points[10]= glm::fvec3(xps, y, zps);
                        std::cout << "IEBIT10 SET\n";
                    }
                    if (intersected_edges & 1 << 11) {
                        // points[11] = VertexInterp(isolevel, v3, v7, pn3, pn7);
                        points[11]= glm::fvec3(xns, y, zps);
                        std::cout << "IEBIT11 SET\n";
                    }

                    for (int n = 0; triTable[config_n][n] != -1; n += 3) {
                        glm::fvec3 p;

                        std::cout << "TRI3:(config_n:" << config_n << ", n:" << n << ") " << triTable[config_n][n    ] << "\n";
                        std::cout << "TRI2:(config_n:" << config_n << ", n:" << n << ") " << triTable[config_n][n + 1] << "\n";
                        std::cout << "TRI1:(config_n:" << config_n << ", n:" << n << ") " << triTable[config_n][n + 2] << "\n";

                        p = points[triTable[config_n][n]];
                        chunk->verts.push_back(mesh::vertex(p));

                        p = points[triTable[config_n][n + 1]];
                        chunk->verts.push_back(mesh::vertex(p));

                        p = points[triTable[config_n][n + 2]];
                        chunk->verts.push_back(mesh::vertex(p));
                    }
                }
            }
        }

        return chunk;
    }
}
