#pragma once

#include <vector>
#include <GLM/glm.hpp>

#include "Mesh.hpp"
#include "marching_cube_table.h"

#DEFINE isolevel 0.5
/*
    This single-function header creates a mesh
*/
namespace mesher
{
    float VertexInterp(
        float p1,
        float p2,
        double valp1,
        double valp2
    ) {
        double mu;
        XYZ p;

        if (glm::abs(isolevel - valp1) < 0.00001) return(p1);

        if (glm::abs(isolevel - valp2) < 0.00001) return(p2);

        if (glm::abs(valp1 - valp2) < 0.00001) return(p1);

        mu = (isolevel - valp1) / (valp2 - valp1);
        p.x = p1.x + mu * (p2.x - p1.x);
        p.y = p1.y + mu * (p2.y - p1.y);
        p.z = p1.z + mu * (p2.z - p1.z);

        return(p);
    }

    float map[2][2][2] = {
        {{1.0f, 0.0f,},{0.0f, 0.0f,}},
        {{0.0f, 0.0f,},{0.0f, 0.0f,}}
    };

    mesh::mesh* march_cubes(
        const glm::vec3& chunk_size,
        float isolevel,
        float (*get_voxels)(float, float, float)
    ) {
        struct Triangles {
            float p[3];
        } triangle;

        struct Grid {
            float p[8];
            double val[8];
        } _grid;

        int i;
        int ntriang;
        int cubeindex;
        float verts[12];
        _grid grid;
        std::vector<triangle> triangles;

        mesh::mesh* chunk = new mesh::mesh();

        grid.p = { 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 };

        /*
         * Find the edge table index that determines which vertices are inside
         * the isosurface.
         */
        cubeindex = 0;
        if (grid.val[0] < isolevel) cubeindex |= 1 << 0;
        if (grid.val[1] < isolevel) cubeindex |= 1 << 1;
        if (grid.val[2] < isolevel) cubeindex |= 1 << 2;
        if (grid.val[3] < isolevel) cubeindex |= 1 << 3;
        if (grid.val[4] < isolevel) cubeindex |= 1 << 4;
        if (grid.val[5] < isolevel) cubeindex |= 1 << 5;
        if (grid.val[6] < isolevel) cubeindex |= 1 << 6;
        if (grid.val[7] < isolevel) cubeindex |= 1 << 7;

        /* Cube is entirely in/out of the surface */
        if (edgeTable[cubeindex] == 0)
            return(0);

        /* Find the vertices where the surface intersects the cube */
        if (edgeTable[cubeindex] & (1 << 0))
            verts[0] = VertexInterp(isolevel, grid.p[0], grid.p[1], grid.val[0], grid.val[1]);
        if (edgeTable[cubeindex] & (1 << 1))
            verts[1] = VertexInterp(isolevel, grid.p[1], grid.p[2], grid.val[1], grid.val[2]);
        if (edgeTable[cubeindex] & (1 << 2))
            verts[2] = VertexInterp(isolevel, grid.p[2], grid.p[3], grid.val[2], grid.val[3]);
        if (edgeTable[cubeindex] & (1 << 3))
            verts[3] = VertexInterp(isolevel, grid.p[3], grid.p[0], grid.val[3], grid.val[0]);
        if (edgeTable[cubeindex] & (1 << 4))
            verts[4] = VertexInterp(isolevel, grid.p[4], grid.p[5], grid.val[4], grid.val[5]);
        if (edgeTable[cubeindex] & (1 << 5))
            verts[5] = VertexInterp(isolevel, grid.p[5], grid.p[6], grid.val[5], grid.val[6]);
        if (edgeTable[cubeindex] & (1 << 6))
            verts[6] = VertexInterp(isolevel, grid.p[6], grid.p[7], grid.val[6], grid.val[7]);
        if (edgeTable[cubeindex] & (1 << 7))
            verts[7] = VertexInterp(isolevel, grid.p[7], grid.p[4], grid.val[7], grid.val[4]);
        if (edgeTable[cubeindex] & (1 << 8))
            verts[8] = VertexInterp(isolevel, grid.p[0], grid.p[4], grid.val[0], grid.val[4]);
        if (edgeTable[cubeindex] & (1 << 9))
            verts[9] = VertexInterp(isolevel, grid.p[1], grid.p[5], grid.val[1], grid.val[5]);
        if (edgeTable[cubeindex] & (1 << 10))
            verts[10] = VertexInterp(isolevel, grid.p[2], grid.p[6], grid.val[2], grid.val[6]);
        if (edgeTable[cubeindex] & (1 << 11))
            verts[11] = VertexInterp(isolevel, grid.p[3], grid.p[7], grid.val[3], grid.val[7]);

        ntriang = 0;
        for (int i = 0; triTable[cubeindex][i] != -1; i += 3) {
            triangles[ntriang].p[0] = verts[triTable[cubeindex][i]];
            triangles[ntriang].p[1] = verts[triTable[cubeindex][i + 1]];
            triangles[ntriang].p[2] = verts[triTable[cubeindex][i + 2]];
            ntriang++;
        }

        return chunk;
    }
}
