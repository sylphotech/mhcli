#include <iostream>
#include <vector>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <GLM/glm.hpp>
#include <GLM/ext.hpp>

#include "mesh.hpp"
#include "marcher.hpp"
#include "generator.hpp"
#include "shader.hpp"
#include "io.hpp"

#define MHASSERT(x, y)          \
    if (!x) {                   \
        std::cerr << y << "\n"; \
        return EXIT_FAILURE;    \
    }

// error callback function.
void glfw_error_callback(int error, const char* message);

bool to_string(std::string& target, const char *filename) {
    std::FILE *fp = std::fopen(filename, "rb");

    if (fp) {
        std::fseek(fp, 0, SEEK_END);
        target.resize(std::ftell(fp));
        std::rewind(fp);
        std::fread(&target[0], 1, target.size(), fp);
        std::fclose(fp);

        return true;
    }

    return false;
}

bool validate_shader(const GLuint& shader) {
    GLint result = 0;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &result);

    if (result == GL_FALSE) {
        GLint length = 0;
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &length);
        GLchar* message = (GLchar*)alloca(length * sizeof(GLchar));
        glGetShaderInfoLog(shader, length, &length, message);
        std::cerr << "[OPENGL]:" << message << "\n";
        glDeleteShader(shader);
        return false;
    }

    return true;
}

bool validate_program(const GLuint& program) {
    GLint result = 0;
    glGetProgramiv(program, GL_LINK_STATUS, &result);

    if (result == GL_FALSE) {
        GLint length = 0;
        glGetProgramiv(program, GL_INFO_LOG_LENGTH, &length);
        GLchar* message = (GLchar*)alloca(length * sizeof(GLchar));
        glGetProgramInfoLog(program, length, &length, message);
        std::cerr << "[OPENGL]:" << message << "\n";
        return false;
    }

    return true;
}

// Prints GL errors
void gl_debug_callback(
    GLenum source,
    GLenum type,
    GLuint id,
    GLenum severity,
    GLsizei length,
    const GLchar *message,
    const void *userParam
) {
    if (severity == GL_DEBUG_SEVERITY_NOTIFICATION)
        return;

    std::cerr << "[OPENGL:(" << source << ", " << type << ", " << id << ", "
        << severity << ")]: " << message << "\n";
}

// Entry point
int main(int argc, char** argv) {
    MHASSERT(glfwInit(), "Failed to initialize GLFW.");
    glfwSetErrorCallback(glfw_error_callback);
    glfwWindowHint(GLFW_RESIZABLE, false);
    glfwWindowHint(GLFW_AUTO_ICONIFY, false);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, true);

    std::cout << "Creating window... " << std::endl;
    GLFWwindow* window = glfwCreateWindow(1280, 720, "Man Hunt Temp", NULL, NULL);

    constexpr int width_center = (1600 - 1280) / 2;
    constexpr int height_center = (900 - 720) / 2;

    MHASSERT(window, "Failed to create window.");
    glfwSetWindowPos(window, width_center, height_center);
    glfwMakeContextCurrent(window);
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    MHASSERT(glewInit() == GLEW_OK, "Failed to initialize GLEW.");

    std::cout << glGetString(GL_VERSION) << "\n";
    glEnable(GL_DEBUG_OUTPUT);
    glDebugMessageCallback(gl_debug_callback, nullptr);
    // glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    float scalar = 1.0f;
    mesh::mesh* mesh = march::run(glm::fvec3(8.0f, 1.0f, 8.0f), scalar);
    std::vector<GLfloat> positions;

    for (const mesh::vertex& vert : mesh->verts) {
        positions.push_back(vert.position.x);
        std::cout << "POS_X: " << vert.position.x << "    ";
        positions.push_back(vert.position.y);
        std::cout << "POS_Y: " << vert.position.y << "    ";
        positions.push_back(vert.position.z);
        std::cout << "POS_Z: " << vert.position.z << "    \n";
    }

    GLuint vao = 0;
    GLuint vbo = 0;
    GLuint position_attrib_index = 0;
    glBindVertexArray(vao);
    glGenVertexArrays(1, &vao);
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, positions.size() * sizeof(GLfloat), positions.data(), GL_STATIC_DRAW);

    GLuint ibo = 0;
    glGenBuffers(1, &ibo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, mesh->indices.size() * sizeof(GLfloat), mesh->indices.data(), GL_STATIC_DRAW);

    // load shaders and compile program
    // GLuint vertexShader     = shader::load("res/shader.vert", GL_VERTEX_SHADER);
    // GLuint fragmentShader   = shader::load("res/shader.frag", GL_FRAGMENT_SHADER);
    //
    // MHASSERT(vertexShader != -1, "Failed to read vertex shader file.");
    // MHASSERT(fragmentShader != -1, "Failed to read fragment shader file.");
    //
    // GLuint program = shader::link(
    //     std::vector<GLuint>{ vertexShader, fragmentShader }
    // );
    //
    // MHASSERT(program != -1, "Shader program validation failed.");
    // glUseProgram(program);
    // glDeleteShader(vertexShader);
    // glDeleteShader(fragmentShader);

    GLuint vertexShader     = glCreateShader(GL_VERTEX_SHADER);
    GLuint fragmentShader   = glCreateShader(GL_FRAGMENT_SHADER);
    std::string vertexSource;
    std::string fragmentSource;

    MHASSERT(to_string(vertexSource, "res/shaders/shader.vert"), "Failed to read vertex shader file.");
    MHASSERT(to_string(fragmentSource, "res/shaders/shader.frag"), "Failed to read fragment shader file.");

    const char* vcstr = vertexSource.c_str();
    const char* fcstr = fragmentSource.c_str();

    glShaderSource(vertexShader, 1, &vcstr, nullptr);
    glCompileShader(vertexShader);
    MHASSERT(validate_shader(vertexShader), "Vertex shader validation failed.");
    glShaderSource(fragmentShader, 1, &fcstr, nullptr);
    glCompileShader(fragmentShader);
    MHASSERT(validate_shader(fragmentShader), "Fragment shader validation failed.");

    GLuint program = glCreateProgram();
    glAttachShader(program, vertexShader);
    glAttachShader(program, fragmentShader);
    glLinkProgram(program);

    MHASSERT(validate_program(program), "Shader program validation failed.");
    glUseProgram(program);
    glDetachShader(program, vertexShader);
    glDetachShader(program, fragmentShader);
    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);

    // compute projection and view
    glm::mat4 projection = glm::perspective(glm::radians(70.0f), 1280.0f / 720.0f, 0.1f, 100.0f);
    glm::vec3 cameraPos(0.0f, 0.0f, 0.0f);
    glm::vec3 cameraFront(0.0f, 0.0f, -1.0f);
    glm::vec3 cameraUp(0.0f, 1.0f, 0.0f);
    glm::mat4 view = glm::lookAt(cameraPos, cameraFront, cameraUp);
    glm::mat4 model = glm::mat4(1.0f);
    GLint projection_location = glGetUniformLocation(program, "u_perspective");
    GLint view_location = glGetUniformLocation(program, "u_view");
    GLint model_location = glGetUniformLocation(program, "u_model");
    glUniformMatrix4fv(projection_location, 1, GL_FALSE, &projection[0][0]);
    glUniformMatrix4fv(view_location, 1, GL_FALSE, &view[0][0]);
    glUniformMatrix4fv(model_location, 1, GL_FALSE, &model[0][0]);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    // glEnable(GL_BLEND);
    glDepthFunc(GL_LESS);
    glCullFace(GL_BACK);
    // glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    int width, height;

    float lastX = 400.0f;
    float lastY = 300.0f;
    float yaw = 0.0f;
    float pitch = 0.0f;

    double xpos = 0;
    double ypos = 0;

    std::size_t indice_count = mesh->indices.size();

    glfwSetCursorPos(window, 1280.0 * 0.5, 720 * 0.5);

    while (!glfwWindowShouldClose(window))
    {
        _sleep(10L);

        // Key pressing for camera movement.
        float cameraSpeed = 0.10f; // adjust accordingly
        if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
            cameraPos += cameraSpeed * cameraFront;
        if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
            cameraPos -= cameraSpeed * cameraFront;
        if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
            cameraPos -= glm::normalize(glm::cross(cameraFront, cameraUp)) * cameraSpeed;
        if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
            cameraPos += glm::normalize(glm::cross(cameraFront, cameraUp)) * cameraSpeed;
        if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS)
            cameraPos += cameraSpeed * cameraUp;
        if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS)
            cameraPos -= cameraSpeed * cameraUp;
        if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
            glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);

        // Handle cursor position.
        {
            glfwGetCursorPos(window, &xpos, &ypos);

            float xoffset = xpos - lastX;
            float yoffset = lastY - ypos;
            lastX = xpos;
            lastY = ypos;

            float sensitivity = 0.1f;
            xoffset *= sensitivity;
            yoffset *= sensitivity;

            yaw   += xoffset;
            pitch += yoffset;

            if(pitch > 89.0f) pitch = 89.0f;
            if(pitch < -89.0f) pitch = -89.0f;
        }

        glm::vec3 front;
        front.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
        front.y = sin(glm::radians(pitch));
        front.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
        cameraFront = glm::normalize(front);
        glm::vec3 looking = cameraPos + cameraFront;

        // Update camera view
        view = glm::lookAt(cameraPos, looking, cameraUp);
        glUniformMatrix4fv(view_location, 1, GL_FALSE, &view[0][0]);

        // Set title.
        std::string camx(std::to_string(cameraPos.x));
        std::string camy(std::to_string(cameraPos.y));
        std::string camz(std::to_string(cameraPos.z));
        std::string lax(std::to_string(looking.x));
        std::string lay(std::to_string(looking.y));
        std::string laz(std::to_string(looking.z));
        std::string campos("Position:(" + camx + ", " + camy + ", " + camz + ")");
        std::string look("Looking at:(" + lax + ", " + lay + ", " + laz + ")");
        std::string title(campos + "    " + look);
        glfwSetWindowTitle(window, title.c_str());

        // query for window dimensions and adjust
        glfwGetFramebufferSize(window, &width, &height);
        glViewport(0, 0, width, height);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        GLint position_location = glGetAttribLocation(program, "position");
        // GLint normal_location = glGetAttribLocation(program, "normal");
        glBindVertexArray(vao);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
        glEnableVertexAttribArray(position_location);
        // glEnableVertexAttribArray(normal_location);
        glVertexAttribPointer(position_location, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), nullptr);
        // glVertexAttribPointer(normal_location, 3, GL_FLOAT, GL_FALSE, 7 * sizeof(GLfloat), (void*)(3 * sizeof(GLfloat)));
        glUseProgram(program);
        glDrawArrays(GL_TRIANGLES, 0, positions.size());
        glDisableVertexAttribArray(position_location);
        // glDisableVertexAttribArray(normal_location);

        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwDestroyWindow(window);
    glfwTerminate();
    return EXIT_SUCCESS;
}

void glfw_error_callback(int error, const char* message) {
    std::cerr << "[GLFW:" << error << "]: " << message << "\n";
}
